//
// Created by fmend on 7/4/2022.
//

#ifndef ENGINE_POSITION_H
#define ENGINE_POSITION_H

struct position {
    float x;
    float y;
};

#endif //ENGINE_POSITION_H
