//
// Created by fmend on 7/4/2022.
//

#ifndef ENGINE_VELOCITY_H
#define ENGINE_VELOCITY_H

struct velocity {
    float dx;
    float dy;
};

#endif //ENGINE_VELOCITY_H
