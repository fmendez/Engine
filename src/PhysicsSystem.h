//
// Created by fmend on 7/4/2022.
//

#ifndef ENGINE_PHYSICSSYSTEM_H
#define ENGINE_PHYSICSSYSTEM_H

#include <entt/entt.hpp>

class PhysicsSystem {
public:
    void update(entt::registry &registry);
};


#endif //ENGINE_PHYSICSSYSTEM_H
