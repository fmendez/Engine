//
// Created by Andres on 27/06/22.
//

#ifndef ENGINE_PLATFORM_H
#define ENGINE_PLATFORM_H

class Platform {

public:
    Platform();
    ~Platform();
    void Update();
    bool isRunning();
};


#endif //ENGINE_PLATFORM_H
